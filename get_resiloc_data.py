import ijs_measurements.client as ijs
import datetime as DT

_BLE_DB_ID = 39
_WIFI_DB_ID = 38
_WIFI_VERBOSE_DB_ID = 37
_WIFI_PARSED_DATA_DB_ID = 25

_begging_of_universe_time = DT.datetime.fromtimestamp(0)  # or 1970.. same thing
_time_str = "%Y-%m-%dT%H:%M:%S.000Z"


def _get_stream(
    ID,
    start_time: DT.datetime = _begging_of_universe_time,
    stop_time: DT.datetime = None,
    convert_timestamp=True,
):
    # if no stop time defined, we want all data until this time in the database
    if stop_time == None:
        stop_time = DT.datetime.utcnow()

    # very verbose conversion of time to make timestamp debug easier
    s1time = start_time.strftime(_time_str)
    s2time = stop_time.strftime(_time_str)
    ijs.set_params([ID], s1time, s2time)
    data = ijs.get_datapoints()

    # simplify data points in-place
    for i, data_point in enumerate(data):
        payload = data_point["payload"]  # temporary

        # keep the timestamp
        ts = data_point["timestamp"]
        if convert_timestamp:
            # but convert to object if so defined
            date_str = ts[:19]
            ts = DT.datetime.strptime(date_str, "%Y-%m-%dT%H:%M:%S")

        t_data = {
            "timestamp": ts,
            "start_timestamp": payload["start_timestamp"],
            "end_timestamp": payload["end_timestamp"],
            "device_location": payload["device"],
            "device_UUID": payload["serial"],
        }
        t_data["devices"] = payload["scanned_device"]

        # replace the data in array with cleaned up data
        data[i] = t_data

    return data


def get_BLE(
    start_time: DT.datetime = _begging_of_universe_time,
    stop_time: DT.datetime = None,
    convert_timestamp=True,
):
    """Get BLE datapoints from the database

    start_time = UTC timepoint from where we want colection of data
    stop_time = UTC timepoint where we want to stop collection
    convert_timestamp = will convert timestamp in object to instance of datetime instead of string
    """
    return _get_stream(
        _BLE_DB_ID, start_time, stop_time, convert_timestamp=convert_timestamp
    )


def get_Wifi(
    start_time: DT.datetime = _begging_of_universe_time,
    stop_time: DT.datetime = None,
    convert_timestamp=True,
):
    """Get WiFi (minimal) datapoints from the database

    start_time = UTC timepoint from where we want colection of data
    stop_time = UTC timepoint where we want to stop collection
    convert_timestamp = will convert timestamp in object to instance of datetime instead of string
    """
    return _get_stream(
        _WIFI_DB_ID, start_time, stop_time, convert_timestamp=convert_timestamp
    )


""" DEPRECATED
def get_Wifi_verbose(
    start_time: DT.datetime = _begging_of_universe_time,
    stop_time: DT.datetime = None,
    convert_timestamp=True,
):
    return _get_stream(
        _WIFI_VERBOSE_DB_ID, start_time, stop_time, convert_timestamp=convert_timestamp
    )
"""


def get_WiFi_verbose_parsed(
    start_time: DT.datetime = _begging_of_universe_time,
    stop_time: DT.datetime = None,
    convert_timestamp=True,
):
    """Get WiFi (verbose) datapoints from the database

    start_time = UTC timepoint from where we want colection of data
    stop_time = UTC timepoint where we want to stop collection
    convert_timestamp = will convert timestamp in object to instance of datetime instead of string
    """
    return _get_stream(
        _WIFI_PARSED_DATA_DB_ID,
        start_time,
        stop_time,
        convert_timestamp=convert_timestamp,
    )


## this will by default get last hour data and print first sample of each result (wil break if nothing is returned)
if __name__ == "__main__":
    stop_time = DT.datetime.utcnow()
    start_time = stop_time - DT.timedelta(hours=1)

    data = get_BLE(start_time, stop_time)
    if len(data) > 0:
        print("all points: ", len(data))
        print(data[0], "\n\n")
    else:
        print("No BLE data!")

    data = get_Wifi(start_time, stop_time)
    if len(data) > 0:
        print("all points: ", len(data))
        print(data[0], "\n\n")
    else:
        print("No Wifi(simple) data!")

    data = get_WiFi_verbose_parsed(start_time, stop_time)
    if len(data) > 0:
        print("all points: ", len(data))
        print(data[0], "\n\n")
    else:
        print("No WiFi (parsed) data!")
