# Simplified data access python script for resiloc project - E6 database

Get data from https://e6-dataproc.ijs.si/landing database

Excerpt from now deprecated repo [https://gitlab.com/e62Lab/resiloc_project/resiloc-data-analysis](https://gitlab.com/e62Lab/resiloc_project/resiloc-data-analysis)




## Usage suggestions: 
 - create venv (e.g. ```python3 -m venv .venv && source .venv/bin/activate``` )
 - install requirements ( e.g. ```pip install --requirements=pip_requirements.txt```)  (requests & requests-future)
 - import into your script (e.g. ```import get_resiloc_data as resiloc```)
 - use ```resiloc.get_BLE(START_TIME, STOP_TIME, True)``` or ```resiloc.get_WiFi()``` or ```resiloc.get_WiFi_verbose_parsed()``` All parameters are optional, and same on all three functions.
   - return value is an array of dictionaries containing
     - timestamp (either datetime.datetime instace or string, depending on what was defined when calling the function)
     - start_timestamp - UNIX timestamp when the scan was started
     - end_timestamp - UNIX timestamp when scan was done
     - device_location - Human readable location
     - device_UUID - unique ID for each device (serial #)
     - devices - actual data receivved (either a dict of mac->rssi, or verbose output of scan packet)